# This file controls options to apply when configuring/building modules, and
# controls which modules are built in the first place.
# List of all options: https://docs.kde.org/trunk5/en/kdesrc-build/kdesrc-build/conf-options-table.html

global
    branch-group kf6-qt6

    # Finds and includes *KDE*-based dependencies into the build.  This makes
    # it easier to ensure that you have all the modules needed, but the
    # dependencies are not very fine-grained so this can result in quite a few
    # modules being installed that you didn't need.
    include-dependencies %{include-dependencies}

    # Install directory for KDE software
    kdedir %{kdedir}

    # Directory for downloaded source code
    source-dir %{source-dir}

    # Directory to build KDE into before installing
    # relative to source-dir by default
    build-dir %{build-dir}

    # qtdir  ~/kde/qt # Where to install Qt6 if kdesrc-build supplies it

    cmake-options -DCMAKE_BUILD_TYPE=RelWithDebInfo

    # kdesrc-build sets 2 options which is used in options like make-options or set-env
    # to help manage the number of compile jobs that happen during a build:
    #
    # 1. num-cores, which is just the number of detected CPU cores, and can be passed
    #    to tools like make (needed for parallel build) or ninja (completely optional).
    #
    # 2. num-cores-low-mem, which is set to largest value that appears safe for
    #    particularly heavyweight modules based on total memory, intended for
    #    modules like qtwebengine
    num-cores %{num_cores}
    num-cores-low-mem %{num_cores_low}

    # kdesrc-build can install a sample .xsession file for "Custom"
    # (or "XSession") logins,
    install-session-driver %{install-session-driver}

    # or add a environment variable-setting script to
    # ~/.config/kde-env-master.sh
    install-environment-driver %{install-environment-driver}

    # Stop the build process on the first failure
    stop-on-failure %{stop-on-failure}

    # Use a flat folder layout under ~/kde/src and ~/kde/build
    # rather than nested directories
    directory-layout flat

    # Use Ninja as cmake generator instead of gmake
    cmake-generator Kate - Ninja

    # Build with LSP support for everything that supports it
    compile-commands-linking %{compile-commands-linking}
    compile-commands-export %{compile-commands-export}

    # Generate .vscode config files in project directories
    # Enable this if you want to use Visual Studio Code for development
    generate-vscode-project-config %{generate-vscode-project-config}
end global

# With base options set, the remainder of the file is used to define modules to build, in the
# desired order, and set any module-specific options.
#
# Modules may be grouped into sets, and this is the normal practice.
#
# You can include other files inline using the "include" command. We do this here
# to include files which are updated with kdesrc-build.

# Common options that should be set for some KDE modules no matter how
# kdesrc-build finds them. Do not comment these out unless you know
# what you are doing.
include %{build_include_dir}/kf6-common-options.ksb

# Qt and some Qt-using middleware libraries. Uncomment if your distribution's Qt
# tools are too old but be warned that Qt take a long time to build!
#include %{build_include_dir}/qt6.ksb
#include %{build_include_dir}/custom-qt6-libs.ksb

# KF6 and Plasma :)
include %{build_include_dir}/kf6-qt6.ksb

# To change options for modules that have already been defined, use an
# 'options' block. See kf6-common-options.ksb for an example

# kate: syntax kdesrc-buildrc;
